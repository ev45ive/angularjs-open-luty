

const utils = angular.module('utils', [])

utils.directive('appStopPropagation', function () {

  return {
    // restrict:'EACM'
    restrict: 'A',
    link(localScope, $element, attrs) {
      // arguments

      $element.on('click', event => {
        event.stopPropagation()
      })
    }
  }
})