const settings = angular.module('settings', [])

settings.controller('SettingsPageCtrl', function ($scope) {

  const vm = $scope.settings = {
    options: {
      name: { label: 'name', value: 'test ' },
      email: { label: 'Email', value: 'test@test.com' },
      website: { label: 'website', value: 'test.com' },
    }
  }

})


settings.directive('appAccordion', function () {

  return {
    transclude: true,
    template: `<div class="accordion"><ng-transclude></ng-transclude></div>`,
    controller() {
      this.tabs = []

      this.open = function (active) {

        this.tabs.forEach(tab => {
          if (tab === active) {
            tab.isOpen = !tab.isOpen;
          } else {
            tab.isOpen = false
          }
        });

      }
    }
  }
})
settings.directive('appCardCollapsible', function () {

  return {
    scope: {
      title: '@'
    },
    transclude: true,
    require: '^^?appAccordion',
    template: `
    <div class="card">
      <div class="card-header">
        <h2 class="mb-0">
          <button class="btn btn-link btn-block text-left" type="button"
                   ng-click="$ctrl.toggle()">
            {{title}}
          </button>
        </h2>
      </div>
      <div class="collapse" ng-class="{show: $ctrl.isOpen}">
        <div class="card-body">
          <ng-transclude></ng-transclude>
        </div>
      </div>
    </div>`,
    link(scope, element, attr, appAccordion) {
      if (!appAccordion) { return }
      appAccordion.tabs.push(scope.$ctrl)
      scope.$ctrl.appAccordion = appAccordion
    },
    // controller:'CardCtrl'
    controllerAs: '$ctrl',
    controller($scope) {
      this.toggle = function () {
        if (this.appAccordion) { this.appAccordion.open(this) }
        else {
          this.isOpen = !this.isOpen
        }
      }
    }
  }
})

settings.directive('settingsTextOption', function ($compile) {

  return {
    // restrict: 'E',
    // scope: true,
    scope: {
      label: '@optionLabel',
      value: '<value',
      onChange: '&onChange'
    },
    require: '?ngModel',
    // templateUrl:'src/settings/settingsTextOption.tpl.html'
    template: `<div class="form-group row">
      <label  class="col-sm-2 col-form-label">
        {{ label }}
      </label>
      <div class="col-sm-10">
        <input type="text" class="form-control" 
                    ng-value="value" 
                    ng-on-input="change(   $event.target.value )">
      </div>
    </div>`,
    link(scope, element, attr, ngModel) {
      scope.change = function (value) { scope.onChange({ $event: value }) }
      if (!ngModel) { return }

      ngModel.$render = function () { scope.value = ngModel.$viewValue }
      scope.change = function (value) {
        ngModel.$setViewValue(value);
      }
    }
  }
})