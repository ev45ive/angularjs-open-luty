

const playlists = angular.module('playlists', [])

playlists.constant('INITIAL_PLAYLISTS_DATA', [])


playlists.service('PlaylistsService', function (
  INITIAL_PLAYLISTS_DATA,
  UserService,
  $http
) {
  console.log('PlaylistsService created')
  const playlists = INITIAL_PLAYLISTS_DATA

  this.getPlaylists = function () {
    return $http.get('https://api.spotify.com/v1/me/playlists')
      .then(resp => resp.data.items)
  }

  this.getPlaylistById = function (id) {
    return playlists.find(p => p.id == id)
  }

  this.createPlaylist = function (draft) {
    return UserService.getUserProfile().then(user => {
      return $http.post(`https://api.spotify.com/v1/users/${user.id}/playlists`, {
        name: draft.name,
        public: draft.public,
        description: draft.description
      }).then(resp => resp.data)
    })
  }

  this.updatePlaylist = function (draft) {
    return $http.put(`https://api.spotify.com/v1/playlists/${draft.id}`, {
      name: draft.name,
      public: draft.public,
      ...(draft.description && { description: draft.description })
    }).then(resp => draft)
  }

  this.deletePlaylist = function (playlist_id) {
    return $http.delete(`https://api.spotify.com/v1/playlists/${playlist_id}/followers`)
  }

  this.savePlaylist = function (draft) {
    draft = angular.copy(draft)
    if (draft.id) {
      return this.updatePlaylist(draft)
    } else {
      return this.createPlaylist(draft)
    }
  }
})



playlists.controller('PlaylistPageCtrl', function (PlaylistsService) {
  console.log('PlaylistPageCtrl created!')

  this.playlists = [];
  this.selected = null;
  this.mode = 'details' /* details | edit */;
  this.message = ''

  this.loadPlaylists = function () {
    return PlaylistsService.getPlaylists()
      .then(data => this.playlists = data).catch(err => {
        this.message = err.message
        throw (err)
      })
  }

  this.loadPlaylists()


  this.toggle = function (selected) {
    this.selected = this.selected == selected ? null : selected
    this.mode = 'details'
  }

  this.updatePlaylist = function (draft) {
    return PlaylistsService.savePlaylist(draft).then(saved => {
      this.selected = saved

      return this.loadPlaylists()
    }).catch(err => {
      this.message = err.message
      throw (err)
    })
  }

  this.createNew = function () {
    this.selected = { name: '', public: false, description: '' }
    this.mode = 'edit'
  }

  this.removePlaylist = function (id) {
    PlaylistsService.deletePlaylist(id).then(() => {
      this.selected = null
      this.mode = 'details'
      return this.loadPlaylists()
    }).catch(err => {
      this.message = err.message
      throw (err)
    })
  }


  this.cancel = function () {
    this.mode = 'details'
  }
  this.save = function (draft) {
    this.mode = 'details'
    this.updatePlaylist(draft)
  }
});


// playlists.controller('PlaylistListCtrl', function ($scope) {
// })

playlists.directive('playlistList', function () {

  return {
    restrict: 'E',
    scope: {
      playlists: '=',
      selected: '=',
      onSelect: '&',
      onDelete: '&',
      onCreate: '&',
    },
    // templateUrl:''
    template: ` <div>
    <div class="list-group">
      <div class="list-group-item" ng-repeat="(index, item) in playlists"
      ng-class=" { 'active': selected.id == item.id } " 
      ng-click="$ctrl.select(item)">
      
        <span>{{index+1}}. {{item.name}}</span>
        <span class="close float-right" app-stop-propagation
        ng-click=" $ctrl.delete(item.id) ">&times;</span>
      </div>
      </div>
      
      <button class="btn btn-info mt-3 float-right" ng-click="onCreate()">Create New Playlist</button>

    </div>`,
    // controller: 'PlaylistListCtrl as $ctrl',
    controllerAs: '$ctrl',
    controller($scope) {
      this.select = function (item) {
        $scope.onSelect({ $event: item })
      }
      this.delete = function (item) {
        $scope.onDelete({ $event: item })
      }
    },
  }
})


/* playlists.directive('playlistDetails', function () {

  return {
    restrict: 'E',
    templateUrl: 'src/playlists/playlist-details.tpl.html',
    scope: {
      playlist: '=',
      onEdit: '&',
    },
    bindToController: true,
    controllerAs: '$ctrl',
    controller(
      // $scope
    ) {
      // this.playlist = $scope.playlist
      // $scope.$watch('playlist', function (newVal) { this.playlist = newVal })

      // this.onEdit = function(){
      //   $scope.onEdit()
      // }
    }
  }
}) */

playlists.component('playlistDetails', {
  templateUrl: 'src/playlists/playlist-details.tpl.html',
  bindings: {
    playlist: '=',
    onEdit: '&',
  },
  controller() {

  }
})

// Angular2:
// @component({
//   templateUrl: '',
//   inputs: ['playlist'],
//   ouptus: ['onEdit'],
// })
// class MyComp { }

// https://code.angularjs.org/1.8.2/docs/guide/component

playlists.component('playlistForm', {
  templateUrl: 'src/playlists/playlist-form.tpl.html',
  bindings: {
    playlist: '=',
    onCancel: '&',
    onSave: '&',
  },
  controller() {

    this.$onInit = function () {
      const { id, name, description, public } = this.playlist
      this.draft = angular.copy({ id, name, description, public })
    }

    this.$postLink = function () {
      this.form
    }

    this.save = function () {
      if (this.form.$invalid) { return }
      
      this.onSave({ $event: this.draft })
    }
  }
})