
const user = angular.module('user', [])



user.factory('UserService', ['$http', '$q', function ($http, $q) {
  
  class UserService {
    user = null;

    // constructor($http, $q) {
    //   this.$http = $http
    //   this.$q = $q
    // }

    getUserProfile() {
      if (this.user) {
        return $q.resolve(this.user)
      }

      return $http.get('https://api.spotify.com/v1/me').then(resp => {
        return this.user = resp.data
      })
    }
  }

  return new UserService($http)
}])

// user.service('UserService', ['$http', '$q', UserService])

// https://github.com/oauthjs/angular-oauth2
// https://github.com/JamesRandall/AngularJS-OAuth2

class AuthService {
  constructor(config) {
    this.config = config
    this.token = ''
  }

  init() {
    const params = new URLSearchParams(window.location.hash.substr(1))
    let token = params.get('access_token')
    if (token) {
      window.location.hash = ''
    }

    if (!token) {
      token = JSON.parse(sessionStorage.getItem('token'))
    }
    if (!token) { return this.login() }

    this.token = token;
    sessionStorage.setItem('token', JSON.stringify(token))
  }

  getToken() {
    return this.token
  }

  login() {
    const {
      authURL,
      client_id,
      response_type,
      redirect_uri,
      show_dialog,
      scope
    } = this.config;

    const params = new URLSearchParams({
      client_id,
      response_type,
      redirect_uri,
      show_dialog,
      scope
    })

    window.location.href = (`${authURL}?${params.toString()}`)
  }
}

user.provider('AuthService', function () {
  let config = {}

  return {
    $get: function () {
      return new AuthService(config)
    },
    configure: function (configuration) {
      config = configuration
      config.scope = config.scopes.join(' ')
    }
  }
})