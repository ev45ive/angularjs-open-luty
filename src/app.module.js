
var app = angular.module('musicApp', [
  'ui.router',
  // 'best-data-grid-ever-plugin',
  'playlists',
  'search',
  'user',
  'settings',
  'utils',
  'musicAppDemo'
])


app.config(function ($stateProvider, $urlServiceProvider) {

  $urlServiceProvider.rules.when('', "/")
  $urlServiceProvider.rules.when('/', "/playlists")
  $urlServiceProvider.rules.otherwise("/")

  $stateProvider.state({
    name: 'playlists',
    url: '/playlists',
    templateUrl: 'src/playlists/playlist-page.tpl.html'
  });

  $stateProvider.state({
    name: 'settings',
    url: '/settings',
    templateUrl: 'src/settings/settings-page.tpl.html'
  });

  $stateProvider.state({
    name: 'search',
    url: '/search',
    component: 'searchPage'
  });

  $stateProvider.state({
    name: 'albums',
    url: '/albums/{album_id}',
    component: 'albumDetailPage',
    resolve: {
      album: function (SearchService,$transition$) {
        return SearchService
          .getAlbumById($transition$.params().album_id)
      }
    }
  });
});


app.controller('AppCtrl', function ($scope) {
  $scope.user = { name: 'User' }

  $scope.wyloguj = function () {
    $scope.user.name = ''
  }

  const navigation = $scope.navigation = {
    pages: [
      { name: 'Playlists', template: 'src/playlists/playlist-page.tpl.html' },
      { name: 'Settings', template: 'src/settings/settings-page.tpl.html' },
      { name: 'Search', template: 'src/search/search-page.tpl.html' },
      { name: 'Album', template: 'src/search/album-detail-page.tpl.html' },
    ],
    activePage: null,
    changePage: function (page) {
      navigation.activePage = page
    }
  }
  navigation.activePage = navigation.pages[0];
  console.log(navigation)


})

app.controller('UserCtrl', function ($scope) { })


// app.controller('PlaylistCtrl', function ($scope) {
//   console.log('Ja nadpisuje orginalny Kontroller')

//   $scope.edit = function () {
//     $scope.pageData.mode = 'edit'
//   }
// })

app.constant('EXAMPLE_CONSTANT', 123)
app.config(function (AuthServiceProvider, EXAMPLE_CONSTANT/* , EXAMPLE_VALUE */) {

  AuthServiceProvider.configure({
    authURL: 'https://accounts.spotify.com/authorize',
    client_id: '54fab7a5a5354d7ca935fb0c62542958',
    response_type: 'token',
    redirect_uri: 'http://localhost:8080/',
    show_dialog: true,
    scopes: [
      'playlist-modify-public',
      'playlist-modify-private',
      'playlist-read-private',
      'playlist-read-collaborative',
    ]
  })

})

app.value('EXAMPLE_VALUE', 123)
app.run(function (AuthService, EXAMPLE_VALUE) {

  AuthService.init()

})

app.config(function ($httpProvider) {
  $httpProvider.interceptors.push(function ($q, AuthService) {
    return {
      'request': function (config) {
        config.headers['Authorization'] = 'Bearer ' + AuthService.getToken()
        return config
      },

      'responseError': function (response) {
        if (response.status === 401) {
          AuthService.login()
        }
        return $q.reject(response.data.error)
      }
    };
  });
})


// ng-app="musicApp"
$(function () {
  angular.bootstrap(document, ['musicApp'])
})

// angular.bootstrap(root1, ['musicApp1'])
// angular.bootstrap(root2, ['musicApp2'])