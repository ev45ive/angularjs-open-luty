
var demo = angular.module('musicAppDemo', [])

demo.constant('INITIAL_PLAYLISTS_DATA', [
  {
    id: 123,
    name: "Angular Greatest Hits",
    favorite: false,
    color: "#ff00ff"
  },
  {
    id: 234,
    name: "Angular TOP20",
    favorite: true,
    color: "#ffff00"
  },
  {
    id: 345,
    name: "Best of Angular",
    favorite: true,
    color: "#00ffff"
  }
])


demo.constant('INITIAL_SEARCH_DATA', [
  {
    id: '123', name: 'Album 123', images: [
      { url: 'https://www.placecage.com/c/300/300' }
    ]
  },
  {
    id: '234', name: 'Album 234', images: [
      { url: 'https://www.placecage.com/c/400/400' }
    ]
  },
  {
    id: '345', name: 'Album 345', images: [
      { url: 'https://www.placecage.com/c/500/500' }
    ]
  },
  {
    id: '456', name: 'Album 456', images: [
      { url: 'https://www.placecage.com/c/250/250' }
    ]
  },

])

/* 
 - search.module.js
 
 - SearchService
    - getSearchResults()

 - SearchPageCtrl
    - search(query = 'batman')
    - getResults() -> INITIAL_SEARCH_DATA


*/