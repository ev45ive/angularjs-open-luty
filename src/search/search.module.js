
const search = angular.module('search', [])

search.constant('INITIAL_SEARCH_DATA', [])


// search.service('SearchService', SearchService)
// function SearchService($q, $http, AuthService) {
//   this.auth = AuthService
//   this.$q = $q;
//   this.$http = $http;

//   this.getAlbumById = function (album_id) {
//     return this.$http.get(`https://api.spotify.com/v1/albums/${album_id}`)
//       .then(resp => resp.data)
//   }

//   this.searchAlbums = function (query) {
//     return this.$http.get('https://api.spotify.com/v1/search', {
//       params: { type: 'album', q: query }
//     }).then(resp => resp.data.albums.items)
//   }
// }

class SearchService {

  constructor($q, $http, AuthService) {
    this.auth = AuthService
    this.$q = $q;
    this.$http = $http;
  }

  getAlbumById(album_id) {
    return this.$http.get(`https://api.spotify.com/v1/albums/${album_id}`)
      .then(resp => resp.data)
  }

  searchAlbums(query) {
    return this.$http.get('https://api.spotify.com/v1/search', {
      params: { type: 'album', q: query }
    }).then(resp => resp.data.albums.items)
  }
}
SearchService.$inject = ['$q', '$http', 'AuthService']
search.service('SearchService', SearchService)


search.component('searchPage', {
  templateUrl: 'src/search/search-page.tpl.html',
  controller: function ($scope, SearchService) {

    this.results = []
    this.query = 'batman'
    this.message = ''


    this.search = function (query) {
      console.log('search', query)

      SearchService.searchAlbums(this.query)
        .then(data => this.results = data)
        .catch(err => this.message = err.message)
    }
    return this
  }
})


search.component('albumDetailPage', {
  bindings: {
    album: '<'
  },
  templateUrl: 'src/search/album-detail-page.tpl.html',
  controller: function ($scope, SearchService) {

    // this.album_id = '5djDvf7OozECsKzEB04uYg';
    // this.album = {};
    // this.message = '';

    this.$ngOnInit = function () {

    }

  }
})