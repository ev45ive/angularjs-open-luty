# GIT
cd .. 
git clone https://bitbucket.org/ev45ive/angularjs-open-luty.git angular_trener
cd angular_trener
npm i 
npm start

# Git pobierz zmiany
git stash -u && git pull -f

# Instalacje
npm -v
node -v
git --version
code -v

https://nodejs.org/en/
https://code.visualstudio.com/


# Npm Pakiet
npm init -y

# BIblioteki
npm search angular

npm install jquery angular bootstrap 
npm i jquery angular bootstrap 

# Narzedzia globalne
npm install --global http-server
<!-- 
C:\Users\<USER>\AppData\Roaming\npm\hs -> C:\Users\<USER>\AppData\Roaming\npm\node_modules\http-server\bin\http-server -->
http-server -p 8080

# Narzdzia lokalne
npm i --save-dev http-server
```json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "http-server --port 8080"
  },
```
npm start
Ctrl+C aby zamknac

# Git
git init 
echo node_modules > .gitignore



# Controller AS syntax
```html
<div ng-controller="PersonCtrl as PersonPageData">
  <!-- $scope.PersonPageData = new PersonCtrl() -->

  <div ng-controller="PersonCtrl as FriendData" ng-repeat="">
    <!-- $scope.FriendData = new PersonCtrl() -->
    <button ng-click="PersonPageData.follow(FriendData.friend.id)">
      Add to parent
    </button>
  </div>

</div>

```

## Console - Scope
angular.element($0).scope()
