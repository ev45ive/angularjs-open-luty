
```js
rootScope = new Scope()
c1 = rootScope.$new()
c2 = rootScope.$new()
rootScope.user = 'test'
```


```js
function Scope(){
    this.id = Scope.prototype.count++
}

Scope.prototype.count = 1 

Scope.prototype.$new = function(){
    var childScope = Object.create(this)
    Scope.apply(childScope)
    return childScope;
}

rootScope = new Scope()
c1 = rootScope.$new()
c2 = rootScope.$new()

rootScope.user = {name:'test'}
c2.user.name === 'test'
true
```