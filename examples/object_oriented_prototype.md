```js
function Person(name ){
    this.name = name, 
    this.sayHello = function(){
        return 'I am ' + name;
    } 
}

alice = new Person('Alice')
alice.sayHello()
"I am Alice"
alice 
```
 Person {
   name: "Alice", 
   sayHello: ƒ
  __proto__: constructor: ƒ **Person(name )**
  __proto__: Object

```js
alice instanceof Person
true
```

##

```js
function Person(name ){
    this.name = name, 

    this.sayHello = function(){
        return 'I am ' + this.name;
    } 
}

alice = new Person('Alice')
console.log(alice.sayHello())

bob = new Person('Bob')
console.log(bob.sayHello())
```

## 

```js

function Person(name ){
    this.name = name, 

    this.sayHello = sayHello
}

function sayHello(){
    return 'I am ' + this.name;
} 

alice = new Person('Alice')
console.log(alice.sayHello())

bob = new Person('Bob')
console.log(bob.sayHello())
// VM7500:12 I am Alice
// VM7500:15 I am Bob
undefined
alice.sayHello == bob.sayHello
true
```
##

```js

function Person(name ){
    this.name = name
}
Person.prototype.sayHello = function sayHello(){
    return 'I am ' + this.name;
} 

alice = new Person('Alice')
console.log(alice.sayHello())

bob = new Person('Bob')
console.log(bob.sayHello())
// VM7739:9 I am Alice
// VM7739:12 I am Bob

alice.sayHello == bob.sayHello
true

bob.sayHello = function(){ return 'I dont talke with uyou~!' }

alice.sayHello()
"I am Alice"

bob.sayHello()
"I dont talke with uyou~!"

```
##

```js
function Employee(name,salary){
    Person.apply(this,arguments)

    this.salary = salary;
}
Employee.prototype = Object.create(Person.prototype)

Employee.prototype.work = function(){
    return 'I wanty my '+this.salary;
}

tom = new Employee('Tom',1200)
Employee {
  name: "Tom", 
  salary: 1200
  __proto__: Person
    work: ƒ ()
      __proto__: sayHello: ƒ sayHello()
      constructor: ƒ Person(name )

tom.sayHello()
"I am Tom"
tom.work()
"I wanty my 1200"

`
```

##

```js
class Person{

    constructor(name){ this.name = name } 

    sayHello(){ return 'I am ' + this.name; }

}

class Employee extends Person{

    constructor(name,salary){ 
        super(name); 
        this.salary = salary; 
    }

    
    work(){ return 'I want ' + this.salary; }

}
undefined
tom = new Employee('Tom', 2500)
tom.work() 
"I want 2500"
tom.sayHello()
"I am Tom"
tom 
Employee {name: "Tom", salary: 2500}
name: "Tom"
salary: 2500
  __proto__: Person
    constructor: class Employee
    work: ƒ work()
      __proto__:
      constructor: class Person
      sayHello: ƒ sayHello()
        __proto__: Object

```


##

```js
tom = new Employee('Tom', 2500)
tom.work() 
"I want 2500"
Person.prototype.superpowers = 'fly!'
"fly!"
tom.superpowers
"fly!"

```