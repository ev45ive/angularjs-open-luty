
```js
$0.oninput = console.log

console.log(1)

setTimeout(()=>console.log(2), 0)

Promise.resolve(3).then(console.log);

now = Date.now()
while( now + 5000 > Date.now() ){ }

console.log(4) 

```


```js
data = jQuery.getJSON('/api/albums.json',{}) // ZLE!!!!

jQuery.getJSON('/api/albums.json',{}, function(data){ 
  console.log(data); 
})
console.log(1)

```

```js

jQuery.getJSON('/api/user',{}, function(data){ 
    console.log(data); 

    jQuery.getJSON(`/api/user/${data.id}/playlists`,function(playlists){
         jQuery.getJSON(`/api/user/${id}/playlists/${playlists[0].id}/tracks`, function(tracks){
             console.log(tracks)

         })
    })
})

```

```js
function getUser(cb){
    jQuery.getJSON('/api/user',{}, function(data){ 
      cb(data)
    })
}

function getPlaylists(cb){
    jQuery.getJSON(`/api/user/${data.id}/playlists`,cb)
}

function getTracks(cb){
    jQuery.getJSON(`/api/user/${id}/playlists/${playlists[0].id}/tracks`, function(tracks){
          cb(tracks)
     })
}

getUser(function(user){ 
    getPlaylists(user, function(playlists){
        getTracks(playlists[0], function(tracks){
            console.log(tracks)
        })
    })
})

```

## Callback hell
```ts
function echo(msg, cb){
    setTimeout(function(){
        cb(msg)
    },2000)
}


echo('Ala', (data) => {
     echo(data + ' ma ', (data) => {
            echo(data + 'kota', (data) => {
         console.log(data)
        }) 
    }) 
}) 

```

## Łącznie promise 
```js
function echo(msg){
    return new Promise((resolveCb)=>{
         setTimeout(function(){
             resolveCb(msg)
         },2000)
    })
}


p = echo('Ala') 

p2 = p.then((data) => {
    return data + ' lubi placki'
})


p3 = p2.then((data) => {
    return data + ' i ma kota '
})

p3.then(console.log) 
// Promise {<pending>}

// Ala lubi placki i ma kota 
```

## Zagnieżdzanie promise

```js

function echo(msg){
    return new Promise((resolveCb)=>{
         setTimeout(function(){
             resolveCb(msg)
         },2000)
    })
}


p = echo('Ala') 

p.then((data) => {
    return echo(data + ' lubi placki')
})
.then((data) => {
    return echo(data + ' i ma kota ')
})
.then(console.log) 

```

## Obsluga bledow

```js
function echo(msg, isErr){
    return new Promise((resolveA, rejectB)=>{
         setTimeout(function(){
             isErr? rejectB(isErr) : resolveA(msg)
         },2000)
    })
}

p = echo('Ala', 'ups..') 

p.then((data) => {  return echo(data + ' lubi placki') })
.then((data) => {     return echo(data + ' i ma kota ') })
.catch(err => 'Wystapil blad ' + err)
.then(console.log) 

```


```js
function echo(msg, isErr){
    return new Promise((resolveA, rejectB)=>{
         setTimeout(function(){
             isErr? rejectB(isErr) : resolveA(msg)
         },2000)
    })
}

p = echo('Ala') 

p.then((data) => {  return echo(data + ' lubi placki') })
.then(() => Promise.reject('blad w trakcie') )
.then((data) => {     return echo(data + ' i ma kota ') })
.catch(err => 'Wystapil blad ' + err)
.then(console.log) 
```